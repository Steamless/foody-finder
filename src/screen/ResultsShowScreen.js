import React, { useState, useEffect } from "react";
import {
  View,
  Linking,
  Text,
  StyleSheet,
  Image,
  FlatList,
  TouchableOpacity,
} from "react-native";

import yelp from "../api/yelp";

const ResultsShowScreen = ({ navigation }) => {
  const [result, setResult] = useState(null);
  const id = navigation.getParam("id");

  //   console.log(result);

  // API GET request
  const getResult = async id => {
    const response = await yelp.get(`/${id}`);
    setResult(response.data);
  };

  // Will call only once
  useEffect(() => {
    getResult(id);
  }, []);

  if (!result) {
    return null;
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{result.name} pictures!</Text>
      <TouchableOpacity>
        <Text style={styles.link} onPress={() => Linking.openURL(result.url)}>
          To {result.name} Website on Yelp
        </Text>
      </TouchableOpacity>
      <FlatList
        data={result.photos}
        keyExtractor={photo => photo}
        renderItem={({ item }) => {
          return <Image style={styles.image} source={{ uri: item }} />;
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
    flex: 1,
  },

  image: {
    width: 300,
    height: 250,
    borderRadius: 10,
    margin: 10,
  },

  title: {
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "center",
  },

  link: {
    fontSize: 20,
    backgroundColor: "#b2f5a9",
    color: "#2c608a",
    alignSelf: "flex-start",
    margin: 10,
    borderWidth: 1,
    borderRadius: 5,
    padding: 5,
  },
});

export default ResultsShowScreen;
