import { useEffect, useState } from "react";
import yelp from "../api/yelp";

export default () => {
  const [results, setResults] = useState([]);
  const [errorMessage, setErrorMessage] = useState("");

  const searchApi = async (searchTerm, searchLocation) => {
    console.log("App started");
    try {
      const response = await yelp.get("/search", {
        params: {
          limit: 50,
          term: searchTerm,
          location: searchLocation,
          // location: "alt duvenstedt",
        },
      });
      setResults(response.data.businesses);
    } catch (error) {
      console.log("error: ", error);
      // setErrorMessage("Something went wrong... ", error);
    }
  };

  // Call searchApi when component is first rendered. BAD CODE!
  // searchApi("pasta");

  // Calls the searchApi just once when the app starts
  useEffect(() => {
    searchApi("club", "leipzig");
  }, []);

  return [searchApi, results, errorMessage];
};
