import React, { useState } from "react";
import { View, Text, StyleSheet, ScrollView } from "react-native";

import SearchBar from "../components/SearchBar";
import SearchLocationInput from "../components/SearchLocationInput";
import useResults from "../hooks/useResults";
import ResultList from "../components/ResultsList";

const SearchScreen = () => {
  const [term, setTerm] = useState("");
  const [location, setLocation] = useState("");
  const [searchApi, results, errorMessage] = useResults();
  console.log(location);

  const filterResultsByPrice = price => {
    // price === '€' || '€€' || '€€€'

    return results.filter(result => {
      return result.price === price;
    });
  };

  return (
    <View style={styles.container}>
      <SearchBar
        term={term}
        onTermChange={setTerm}
        onTermSubmit={() => searchApi(term, location)}
      />
      <SearchLocationInput
        location={location}
        onLocationChange={setLocation}
        onLocationSubmit={() => searchApi(term, location)}
      />
      {errorMessage ? (
        <Text style={styles.testStyle}>{errorMessage}</Text>
      ) : null}

      {/* <Text style={styles.testStyle}>
        We have found {results.length} results
      </Text> */}
      <ScrollView>
        <ResultList
          results={filterResultsByPrice("€")}
          title="Chillig-billig"
        />
        <ResultList
          results={filterResultsByPrice("€€")}
          title="Genau passend"
        />
        <ResultList
          results={filterResultsByPrice("€€€")}
          title="Baby, I got your money!"
        />

        <ResultList
          results={filterResultsByPrice("€€€€")}
          title="Big spender $$$!"
        />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
    flex: 1,
  },

  testStyle: {
    fontSize: 20,
  },

  resultListStyle: {},
});

export default SearchScreen;
