import React from "react";
import { View, TextInput, StyleSheet } from "react-native";

const SearchBar = ({ location, onLocationChange, onLocationSubmit }) => {
  return (
    <View style={styles.backgroundStyle}>
      <TextInput
        style={styles.inputStyle}
        autoCapitalize="none"
        autoCorrect={false}
        placeholderTextColor="#5cb893"
        placeholder="Where are you now?"
        value={location}
        onChangeText={onLocationChange}
        onEndEditing={onLocationSubmit}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  backgroundStyle: {
    backgroundColor: "#b2f5a9",
    height: 35,
    borderRadius: 10,
    marginHorizontal: 15,
    alignItems: "flex-start",
  },

  inputStyle: {
    fontSize: 20,
    paddingHorizontal: 5,
    color: "#5cb893",
  },
});

export default SearchBar;
