import React from "react";
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity,
} from "react-native";
import { withNavigation } from "react-navigation";

import ResultsDetail from "./ResultsDetail";

const ResultList = ({ title, results, navigation }) => {
  if (!results.length) {
    return null;
  }

  return (
    <View styles={styles.container}>
      <Text style={styles.title}>{title}</Text>
      {/* <Text style={styles.title}>{results.length}</Text> */}
      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        data={results}
        keyExtractor={result => result.id}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity
              onPress={() => navigation.navigate("Results", { id: item.id })}
            >
              <ResultsDetail result={item} />
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    // backgroundColor: "#56d",
    // marginBottom: 10,
  },

  title: {
    fontSize: 20,
    fontWeight: "bold",
    margin: 5,
  },
});

export default withNavigation(ResultList);
